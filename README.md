# Présentation des déploiements avec k8s

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Introduction aux déploiements dans Kubernetes

Nous allons parler des déploiements. Cette leçon va fournir une vue d'ensemble sur ce que sont les déploiements dans Kubernetes.

## 1. Qu'est-ce qu'un déploiement ?

Un **déploiement** est un objet Kubernetes qui définit un état désiré pour un ensemble de répliques. 

Une **réplique** est un ensemble de pods identiques exécutant les mêmes conteneurs avec la même configuration. En gros, vous pouvez voir une réplique comme plusieurs copies du même pod. Le déploiement définit l'état souhaité pour cet ensemble de pods répliqués, et le contrôleur de déploiement veille à maintenir cet état désiré en créant, supprimant et remplaçant des pods avec de nouvelles configurations.

## 2. Concept d'état désiré

Le déploiement permet de définir un état désiré pour les pods répliqués. Si vous créez un déploiement avec **trois répliques**, l'état désiré est d'avoir trois copies de votre pod. Le contrôleur de déploiement créera ces trois pods pour maintenir cet état désiré.

Quelques champs importants dans l'état désiré d'un déploiement :

- **Replicas** : Définit le nombre de pods répliqués que le déploiement va chercher à maintenir.

- **Selector** : Un sélecteur de labels utilisé pour identifier les pods gérés par le déploiement.

- **Template** : Un modèle de pod utilisé pour créer les pods gérés par ce déploiement. 

## 3. Cas d'utilisation des déploiements

Les déploiements peuvent être utilisés pour :

- **Faire évoluer une application** : En changeant le nombre de répliques, vous pouvez facilement augmenter ou réduire le nombre de pods exécutés.

- **Effectuer des mises à jour progressives** : Vous pouvez mettre à jour la version du logiciel de manière progressive sans interruption de service.

- **Revenir à une version précédente** : Si une nouvelle version présente des problèmes, vous pouvez revenir facilement à la version précédente.

## 4. Démonstration pratique

Créons un déploiement pour voir comment cela fonctionne.

1. **Connexion au serveur de contrôle**

   Connectez-vous à votre serveur de contrôle Kubernetes.

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS
```

2. **Création du fichier de déploiement**

   Créez un fichier YAML pour le déploiement.

```sh
vi mydeployment.yml
```

   Ajoutez le contenu suivant au fichier :

   ```yaml
   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: my-deployment
   spec:
     replicas: 3
     selector:
       matchLabels:
         app: my-deployment
     template:
       metadata:
         labels:
           app: my-deployment
       spec:
         containers:
         - name: nginx
           image: nginx:1.19.1
           ports:
           - containerPort: 80
   ```

   Enregistrez et quittez le fichier

3. **Création du déploiement**

   Créez le déploiement à partir du fichier YAML.

```sh
kubectl create -f mydeployment.yml
```

   Vous verrez un message indiquant que le déploiement a été créé avec succès.

4. **Vérification du déploiement**

   Vérifiez le déploiement.

```sh
kubectl get deployments
```

   Vous verrez votre déploiement avec le nombre de répliques prêtes et à jour.

5. **Suppression d'un pod de déploiement**

   Supprimez l'un des pods du déploiement.

```sh
kubectl delete pod <pod-name>
```

   Vérifiez à nouveau les pods.

```sh
kubectl get pods
```

   Vous verrez que le pod supprimé a été recréé pour maintenir l'état désiré de trois répliques.

# Conclusion

Pour récapituler, nous avons répondu à la question "Qu'est-ce qu'un déploiement ?", parlé du concept d'état désiré, évoqué quelques cas d'utilisation des déploiements, et réalisé une démonstration pratique de la création d'un déploiement. C'est tout pour cette leçon. À la prochaine !


# Reférences

https://kubernetes.io/docs/concepts/workloads/controllers/deployment/